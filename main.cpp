#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
using namespace std;

int main()
{
    freopen("output.txt", "wt", stdout);
    freopen("input.txt", "rt", stdin);
    string s, s1;
    cin >> s >> s1;

    vector <vector <int> > mas (s.length()+1,
                                vector <int> (s1.length()+1, 0));
    stack < char> likeness;

    for(int i = 0; i <=(int) s.length(); i++)
        mas[i][0] = i;
    for(int i = 0; i <= (int)s1.length(); i++)
        mas[0][i] = i;

    for(int i = 1; i <= (int)s.length(); i++)
    {
        for(int j = 1; j <= (int)s1.length(); j++ )
        {
            if(s[i - 1] == s1[j - 1])
            {
                mas[i][j] = mas[i - 1][j - 1];
            }
            else
            {
                mas [i][j] = min(min(mas[i - 1][j], mas[i][j - 1]),
                        mas[i-1][j-1]) + 1;


            }
        }
    }

    for(int i = 0; i <= (int)s.length(); i++)
    {
        cout << endl;
        for(int j = 0; j <= (int)s1.length();j++ )
            cout << mas[i][j]<<" ";

    }
    cout << endl;

    int i = s.length();
    int j = s1.length();
    int ins, rep, del;
    while(i != 0 || j != 0 )
    {

            if(i >= 1 && j >=1&&s[i - 1] == s1[j - 1])
            {
                likeness.push('M');
                --i; --j;
            }
            else
            {   if(j >= 1)
                     ins = mas[i][j - 1];
                if(i >= 1 && j >=1)
                     rep = mas[i - 1][j - 1];
                if(i >= 1)
                     del = mas[i - 1][j];

                if(j >= 1 && mas[i][j] == ins + 1)
                {
                    likeness.push('I');
                    --j;
                }
                else if (i >= 0 && mas[i][j] == del + 1)
                {
                    likeness.push('D');
                    --i;
                }
                else if (i >= 1 && j >=1 && mas[i][j] == rep + 1)
                {
                    likeness.push('R');
                    --i; --j;
                }
            }
    }
    stack <char> st1 = likeness;
    cout << mas[s.length()][s1.length()] << endl;
    cout << s << endl << s1 << endl;
    while(!likeness.empty())
    {
        cout << likeness.top();
        likeness.pop();
    }
    cout << endl;

    j = 0;


    cout << s << " --> ";
    while(!st1.empty())
    {
        if(st1.top() == 'I')
        {
            s.insert(j, 1,s1[j]);
            j++;
            cout << s << " --> ";
        }

        else if(st1.top() == 'R')
        {
            s[j] = s1[j];
            j++;
            cout << s << " --> ";
        }
        else if(st1.top() == 'D')
        {
            s.replace(j,1,"");
            cout << s << " --> ";
        }
        else if(st1.top() == 'M')
        {
            j++;
        }

        st1.pop();

    }

    return 0;
}


